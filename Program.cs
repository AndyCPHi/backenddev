using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using static AndyBackendTest.Model;

namespace AndyBackendTest
{
    public class Program
    {
        public static List<StockCheck> allStockChecks = new List<StockCheck> { };
        public static List<int> blacklist = new List<int> { 1, 3, 5, 7, 9 };

    public static void Main(string[] args)
        {
            // CreateHostBuilder(args).Build().Run();
            UnitTesting();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        // Checks item is (1) in list, (2) not in blacklist & (3) matching reg or chassis number
        // Else, return null
        public static StockCheckItem MatchingEngineItems(StockCheckItem sampleItem, List<StockCheckItem> testArray, 
            List<int> blacklist = null)
        {

            if (testArray.Count < 1)
            {
                return null;
            }

            foreach (StockCheckItem item in testArray)
            {

                if (item != null & !InBlackList(item.Id, blacklist))
                {

                    // Reg match
                    if (item.Reg == sampleItem.Reg)
                    {
                        return item;
                    }

                    // Chasis match
                    if (item.Chassis == sampleItem.Chassis)
                    {
                        return item;
                    }

                }
            }

            return null;
        }

        // Similar function but for scans, not sure how to merge with above.
        public static Scan MatchingEngineScans(StockCheckItem sampleItem, List<Scan> testArray, List<int> blacklist = null)
        {

            if(testArray == null || testArray.Count < 1)
            {
                return null;
            }

            foreach (Scan item in testArray)
            {
                System.Diagnostics.Trace.WriteLine(item.Id);

                if (item != null & !InBlackList(item.Id, blacklist))
                {
                    System.Diagnostics.Trace.WriteLine("Item not null & not in blacklist.");
                    System.Diagnostics.Trace.WriteLine(item.Reg, sampleItem.Reg);

                    // Reg match
                    if (item.Reg == sampleItem.Reg)
                    {
                        System.Diagnostics.Trace.WriteLine("Reg match.");
                        return item;
                    }

                    // Chasis match
                    if (item.Chassis == sampleItem.Chassis)
                    {
                        System.Diagnostics.Trace.WriteLine("Chassis match.");
                        return item;
                    }

                }
            }

            return null;
        }

        // If input string empty, return full scan list
        // If input string "low", return scans with RegConfidence < 80
        // Otherwise, return scans matching text input
        // Also, order results by scan date
        public static List<Scan> SearchScan(string inputString, List<Scan> sourceScans)
        {
            if (inputString == "")
            {
                return sourceScans;
            }

            List<Scan> results = new List<Scan> { };
            string searchTerm = inputString.ToLower();

            if (searchTerm == "low")
            {
                results = sourceScans.FindAll(scan => scan.RegConfidence < 80);
            }
            else
            {
                foreach (Scan scan in sourceScans)
                {

                    if (
                         scan.Description != null && scan.Description.ToLower().Contains(searchTerm) ||
                         scan.Reg != null && scan.Reg.ToLower().Contains(searchTerm) ||
                         scan.Chassis != null && scan.Chassis.ToLower().Contains(searchTerm) ||
                         scan.Location != null && scan.Location.Description.ToLower().Contains(searchTerm)
                        )
                    {
                        results.Add(scan);
                    }

                }

            }

            results.OrderBy(x => x.ScanDate);
            return results;
        }

        // Takes a search term (string) and returns any stockcheck items containing that search term in the
        // description, reg, chassis or comment. If >1, entries are sorted by Id.
        public static List<StockCheckItem> SearchStockCheckItems(string inputTerm, List<StockCheckItem> itemsToSearch)
        {
            string searchTerm = inputTerm.ToLower();
            List<StockCheckItem> results = new List<StockCheckItem> { };

            foreach (StockCheckItem item in itemsToSearch)
            {

                if ( 
                     item.Description != null && item.Description.ToLower().Contains(searchTerm) ||
                     item.Comment != null && item.Comment.ToLower().Contains(searchTerm) ||
                     item.Reg != null && item.Reg.ToLower().Contains(searchTerm) ||
                     item.Chassis != null && item.Chassis.ToLower().Contains(searchTerm)
                    )
                {
                    results.Add(item);
                }

            }

            // Sorts list by Id
            results.OrderBy(x => x.Id);
            return results;
        }

        // Helper function: checks if StockCheckItem id is in blacklisted ids
        public static bool InBlackList(int idNumber, List<int> blacklist)
        {
            return blacklist.Contains(idNumber);
        }

        // Returns StockCheckItems where either
        // 1. Chassis numb does not equal attached scan chassis number
        // 2. Reg numb does not equal attached scan reg number
        public static List<StockCheckItem> ReturnVehiclesWithMismatchedRegOrChassis(List<StockCheckItem> itemsToSearch)
        {
            List<StockCheckItem> dmsStockScanned = itemsToSearch.FindAll(x => x.IsInStock && x.IsScanned);
            List<StockCheckItem> problems = new List<StockCheckItem> { };

            foreach (StockCheckItem item in dmsStockScanned)
            {

                if(IsRegOrChassisMismatched(item))
                {
                    problems.Add(item);
                }

            }

            return problems;
        }

        private static bool IsRegOrChassisMismatched(StockCheckItem inputItem)
        {

            if (inputItem.Scan != null)
            {
                if (inputItem.Reg != null && inputItem.Reg != "" && inputItem.Reg != "" &&
                    inputItem.Reg != "INPUT" && inputItem.Scan != null && inputItem.Scan.Reg != "INPUT")
                {
                    if (inputItem.Reg != inputItem.Scan.Reg)
                    {
                        inputItem.MismatchReg = true;
                        return true;
                    }

                }

                if (inputItem.Chassis != inputItem.Scan.Chassis)
                {
                    inputItem.MismatchChassis = true;
                    return true;
                }
            }

            return false;
        }

        // Part 1 of MatchingProcess function
        // 1. Go through every StockCheckItem in Stockcheck
        // 2. If scan is matched to StockCheck scans, add ScanId to int array.
        // 3. Otherwise search other site scans for a match
        // 4. Lastly search reconciling items (missing vehicles) for a match
        // If a match is found, details such as recon codes are updated.
        public static List<int> GetMatchingScanIds(StockCheck stockcheckIn, List<Scan> otherSiteScans,
            List<StockCheckItem> reconcilingItemsListForMissingVehicles)
        {
            // Blacklist
            List<int> scansUsed = new List<int> { };
            List<StockCheckItem> itemsToSearch = stockcheckIn.GetItemsInStock();

            foreach (StockCheckItem item in itemsToSearch)
            {
                System.Diagnostics.Trace.WriteLine("Getting matching scans...");
                Scan matchingScan = MatchingEngineScans(item, stockcheckIn.Scans, scansUsed);

                if(matchingScan == null)
                {
                    continue;
                }

                // If scan not already matched
                if(matchingScan.Id != item.ScanId)
                {
                    System.Diagnostics.Trace.WriteLine("Assigning matching scan...");
                    AssignMatchingScanToItem(item, matchingScan);
                    scansUsed.Add(matchingScan.Id);
                }
                else
                {
                    Scan matchingScanFromOtherSiteScans = null;

                    // Not 100% on meaning behind logic for this?
                    if (stockcheckIn.IsTotal == false)
                    {
                        matchingScanFromOtherSiteScans = MatchingEngineScans(item, otherSiteScans);
                    }

                    // If a match is found at another site...
                    if(matchingScanFromOtherSiteScans != null)
                    {
                        // Get location description from the Id number
                        StockCheck matchingStockCheck = GetStockCheckFromId(matchingScanFromOtherSiteScans.ScanSession.Id);

                        string siteDescription = Constants.GetSite.GetSiteNameFromId(matchingStockCheck.SiteCode);
                        string locationDescription = Constants.GetLocation.GetLocationNameFromId(matchingScanFromOtherSiteScans.LocationCode);

                        item.ScanId = matchingScanFromOtherSiteScans.Id;
                        item.MatchingStockCheckItem = CreateMatchingStockcheckItem(matchingScanFromOtherSiteScans, siteDescription, locationDescription);
                    }
                    else 
                    {
                        // If a match is found in Missing Vehicles...
                        StockCheckItem matchingItem = MatchingEngineItems(item, reconcilingItemsListForMissingVehicles);

                        if(matchingItem != null)
                        {
                            item.ReconcilingItemTypeCode = matchingItem.ReconcilingItemTypeCode;
                            item.ReconcilingItemType = Constants.SetReconcilingType.GetMissingType();
                            item.MatchingStockCheckItem = matchingItem;
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }

            return scansUsed;
        }

        // Part 2
        // Matches scans to stock to find those that are missing
        // If they're missing on DMS stock records, add them into stockcheckitems
        public static void GetMissingItems(StockCheck stockcheckIn, List<Scan> otherSiteScans,
    List<StockCheckItem> reconcilingItemsListForMissingVehicles)
        {
            List<StockCheckItem> newUnknownCars = new List<StockCheckItem> { };
            List<int> stockCheckItemIdsUsedToMatchScanTo = new List<int> { };
            List<int> alreadyFoundDuplicateScanIds = new List<int> { };

            foreach(Scan scan in stockcheckIn.Scans)
            {
                scan.IsDuplicate = false;

                // Checks input isn't null, INPUT, " " or "" 
                bool lookForDuplicateBasedOnReg = duplicateCheckHelper(scan.Reg);
                bool lookForDuplicateBasedOnChassis = duplicateCheckHelper(scan.Chassis);

                List<Scan> alreadyExisting = new List<Scan> { };

                // Checking for duplicates...
                foreach (Scan s in stockcheckIn.Scans)
                {
                    CheckForDuplicateScan(lookForDuplicateBasedOnReg, lookForDuplicateBasedOnChassis, s, scan, alreadyExisting);
                }

                // Eliminate dupes from list?
                if (alreadyExisting.Count > 0)
                {
                    scan.IsDuplicate = true;
                    alreadyFoundDuplicateScanIds.Add(scan.Id);
                    newUnknownCars.Add(CreateUnknownCar(scan, stockcheckIn));
                    continue;
                }

                StockCheckItem scanToItem = GetScanFromItem(scan);
                StockCheckItem matchingStockCheckItemInStock = MatchingEngineItems(scanToItem, reconcilingItemsListForMissingVehicles);
            }


        }

        // Helper functions

        // Used to matching process
        private static StockCheckItem GetScanFromItem(Scan inputScan)
        {
            StockCheckItem result = new StockCheckItem
            {
                // This is non nullable?
                Id = null,
                Reg = inputScan.Reg,
                Chassis = inputScan.Chassis

            };

            return result;
        }

        // This needs to be completed properly & data type checked.
        private static StockCheckItem CreateUnknownCar(Scan scan, StockCheck stockCheckIn)
        {
            StockCheckItem result = new StockCheckItem
            {
                // This is non nullable?
                Id = null,

                // Should be whatever duplicate code is... placeholder for now
                ReconcilingItemTypeCode = 2,
                ScanId = scan.Id,
                Scan = scan,
                StockCheckId = stockCheckIn.Id,
                Description = scan.Description,
                IsProblem = false,
                Reg = scan.Reg,
                Chassis = scan.Chassis,
                IsScannedOrInStock = true,
                IsInStock = false,
                IsScanned = true,
                IsResolved = true

            };

            return result;
        }

        private static bool CheckForDuplicateScan(bool lookForDuplicateBasedOnReg, bool lookForDuplicateBasedOnChassis, Scan scanOne, Scan scanTwo, List<Scan> alreadyExisting)
        {

            // Same scan, ignore
            if (scanOne.Id == scanTwo.Id)
            {
                return false;
            }

            // Check for Reg/Chassis Duplicate
            if (lookForDuplicateBasedOnReg == true)
            {
                if (scanOne.Reg == scanTwo.Reg)
                {
                    alreadyExisting.Add(scanOne);
                    return true;
                }
            }

            if (lookForDuplicateBasedOnChassis == true)
            {
                if (scanOne.Chassis == scanTwo.Chassis)
                {
                    alreadyExisting.Add(scanOne);
                    return true;
                }
            }

            return false;
        }

        private static StockCheckItem CreateMatchingStockcheckItem(Scan matchingScanFromOtherSiteScans, string siteDescription, string locationDescription)
        {

            StockCheckItem result = new StockCheckItem
            {
                ReconcilingItemTypeCode = -1,
                Description = matchingScanFromOtherSiteScans.Reg + " | " + matchingScanFromOtherSiteScans.Chassis,
                IsProblem = false,
                Reg = matchingScanFromOtherSiteScans.Reg,
                Chassis = matchingScanFromOtherSiteScans.Chassis,
                IsScannedOrInStock = true,
                IsInStock = true,
                IsScanned = true,
                Comment = "At " + siteDescription + " " + locationDescription,
                IsResolved = true

            };

            return result;
        }

        private static bool duplicateCheckHelper(string regOrChassis)
        {
            if (regOrChassis != null && regOrChassis != "INPUT" && regOrChassis != "" && regOrChassis != " ")
            {
                return true;
            }

            return false;
        }


        private static void AssignMatchingScanToItem(StockCheckItem item, Scan matchingScan)
        {
            item.ScanId = matchingScan.Id;
            item.Scan = matchingScan;
            item.IsScanned = true;
            item.ReconcilingItemTypeCode = 1;
        }

        public static StockCheck GetStockCheckFromId(int searchId)
        {
            foreach(StockCheck stockCheck in allStockChecks)
            {
                if(stockCheck.Id == searchId)
                {
                    return stockCheck;
                }
            }

            return null;
        }

        public static StockCheckItem GetItemFromStockCheckWithId(StockCheck stockCheck, int itemId)
        {
            foreach (StockCheckItem item in stockCheck.StockCheckItems)
            {
                if (item.Id == itemId)
                {
                    return item;
                }
            }

            return null;
        }

        private static void CreateDummyStockChecks()
        {
            var testStockCheck = new StockCheck
            {
                Id = 1,
                Date = new DateTime(2019, 6, 1, 7, 47, 0),
                SiteCode = 1,
                IsTotal = false
            };

            var testStockCheck2 = new StockCheck
            {
                Id = 2,
                Date = new DateTime(2018, 6, 1, 7, 47, 0),
                SiteCode = 3,
                IsTotal = false
            };

            allStockChecks.Add(testStockCheck);
            allStockChecks.Add(testStockCheck2);

            Debug.Assert(GetStockCheckFromId(1).SiteCode == 1);
            Debug.Assert(GetStockCheckFromId(2).SiteCode == 3);
            Debug.Assert(GetStockCheckFromId(2).IsTotal == false);

        }

        public static void HelperFunctionTesting()
        {
            // Blacklist Checker
            Debug.Assert(InBlackList(3, blacklist) == true);
            Debug.Assert(InBlackList(2, blacklist) == false);
            Debug.Assert(InBlackList(99999999, blacklist) == false);
            Debug.Assert(InBlackList(-1, blacklist) == false);

            // GetSiteNameFromId
            Debug.Assert(Constants.GetSite.GetSiteNameFromId(1) == "Audi London");
            Debug.Assert(Constants.GetSite.GetSiteNameFromId(2) == "Renault Bristol");
            Debug.Assert(Constants.GetSite.GetSiteNameFromId(3) == "Renault Wolverhampton");
        }

        public static void UnitTesting()
        {

            System.Diagnostics.Trace.WriteLine("Testing started.");

            HelperFunctionTesting();
            CreateDummyStockChecks();

            var missingItemType = new ReconcilingItemType
            {
                ExplainsUnknownVehicle = false,
                ExplainsMissingVehicle = true
            };

            var unknownItemType = new ReconcilingItemType
            {
                ExplainsUnknownVehicle = true,
                ExplainsMissingVehicle = false
            };

            var noItemType = new ReconcilingItemType
            {
                ExplainsUnknownVehicle = false,
                ExplainsMissingVehicle = false
            };

            StockCheckItem testItem = Model.StockCheckItem.CreateDummyItem(4, "Outside", "Andy's Comment", "AZ 1249", "IKSDSF1245", true, true, noItemType);

            List<Location> locationList = new List<Location> { };

            locationList.Add(new Location(1, "Luton"));
            locationList.Add(new Location(2, "Bristol"));
            locationList.Add(new Location(3, "Wolverhampton"));

            List <StockCheckItem> testArray = new List<StockCheckItem> { 
                Model.StockCheckItem.CreateDummyItem(1, "Car 123", "Test comment", "NHK 128", "IKFESF1245", true, true, missingItemType),
                Model.StockCheckItem.CreateDummyItem(2, "Outside", "Thursday", "AZ 1249", "IKSDSF1245", true, true, missingItemType),
                Model.StockCheckItem.CreateDummyItem(5, null, null, "AI 2249", "IKSDSF1545", true, true, unknownItemType),
                Model.StockCheckItem.CreateDummyItem(3, "Garage", "Friday", "AZ 1349", "EKSDSF1245", true, true, noItemType),
                Model.StockCheckItem.CreateDummyItem(8, "None", "Friday", "AJ 1349", "EKSDSF8245", false, true, noItemType),

            };

            // Assigning these items to first StockCheck
            allStockChecks.ElementAt(0).StockCheckItems = testArray;

            // Maching Engine
            StockCheckItem resultItem = MatchingEngineItems(testItem, testArray, blacklist);

            Debug.Assert(resultItem.Id == 2);
            Debug.Assert(resultItem.Reg == "AZ 1249");
            Debug.Assert(resultItem.Chassis == "IKSDSF1245");

            // SearchStockCheckItems
            List<StockCheckItem> testArray2 = SearchStockCheckItems("Garage", testArray);
            var firstElement = testArray2.First();

            Debug.Assert(testArray2.Count == 1);
            Debug.Assert(firstElement.Id == 3);
            Debug.Assert(firstElement.Description == "Garage");

            List<StockCheckItem> testArray3 = SearchStockCheckItems("Garage1", testArray);
            Debug.Assert(testArray3.Count == 0);

            List<StockCheckItem> testArray4 = SearchStockCheckItems("AZ", testArray);
            Debug.Assert(testArray4.Count == 2);

            firstElement = testArray4.First();
            Debug.Assert(firstElement.Id == 2);
            Debug.Assert(testArray4.ElementAt(1).Id == 3);

            // Search Scan
            Scan testScan = new Scan
            {
                Id = 52,
                Reg = "AZ 1249",
                RegConfidence = 75,
                Chassis = "IKSDSF1245",
                Description = "Test scan.",
                Location = locationList.ElementAt(1),

                // 6/1/2008 7:47:00 AM
                ScanDate = new DateTime(2019, 6, 1, 7, 47, 0)
            };

            List<Scan> otherSiteScans = new List<Scan> { };

            otherSiteScans.Add(new Scan
            {
                Id = 53,
                Reg = "AZ 1249",
                RegConfidence = 75,
                Chassis = "IKSDSF1245",
                Description = "Test scan.",
                Location = locationList.ElementAt(1),

                // 6/1/2008 7:47:00 AM
                ScanDate = new DateTime(2019, 6, 1, 7, 47, 0),
                ScanSession = new ScanSession(52, 2)
            });

            otherSiteScans.Add(new Scan
            {
                Id = 55,
                Reg = "AZ 1249",
                RegConfidence = 75,
                Chassis = "IKSDSF1245",
                Description = "Test scan.",
                Location = locationList.ElementAt(2),

                // 6/1/2008 7:47:00 AM
                ScanDate = new DateTime(2019, 6, 1, 7, 47, 0),
                ScanSession = new ScanSession(52, 2)
            });

            allStockChecks.ElementAt(0).Scans = otherSiteScans;
            Debug.Assert(testScan.Id == 52);
            Debug.Assert(testScan.Location.Description == "Bristol");

            Scan resultScan = MatchingEngineScans(testItem, otherSiteScans, blacklist);
            Debug.Assert(resultScan != null);

            testArray.ElementAt(0).Scan = testScan;
            Debug.Assert(testArray.ElementAt(0).Scan.Id == 52);
            Debug.Assert(testArray.ElementAt(0).Scan.Location.Description == "Bristol");

            testArray.ElementAt(1).Scan = testScan;

            // Mismatched chassis/reg ids
            Debug.Assert(IsRegOrChassisMismatched(testArray.ElementAt(0)) == true);
            Debug.Assert(IsRegOrChassisMismatched(testArray.ElementAt(1)) == false);

            List<StockCheckItem> reconcilingItemsListForMissingVehicles = ReturnVehiclesWithMismatchedRegOrChassis(testArray);
            Debug.Assert(reconcilingItemsListForMissingVehicles.Count == 1);

            List<StockCheckItem> testArray6 = allStockChecks.ElementAt(0).GetItemsInStock();
            Debug.Assert(testArray6.Count == 4);

            List<StockCheckItem> testArray7 = allStockChecks.ElementAt(0).GetMissingVehicles();
            Debug.Assert(testArray7.Count == 2);

            testArray7 = allStockChecks.ElementAt(0).GetUnknownVehicles();
            Debug.Assert(testArray7.Count == 1);

            // Getting Descriptions from Ids
            Debug.Assert(Constants.GetLocation.GetLocationNameFromId(1) == "London");
            Debug.Assert(Constants.GetSite.GetSiteNameFromId(2) == "Renault Bristol");

            var result = GetMatchingScanIds(allStockChecks.ElementAt(0), otherSiteScans, reconcilingItemsListForMissingVehicles);
            Debug.Assert(result.Count() == 1);

            // Checks the matching stockcheck has had its details changed
            var matchingItem = GetItemFromStockCheckWithId(allStockChecks.ElementAt(0), 2);
            Debug.Assert(matchingItem != null);
            Debug.Assert(matchingItem.IsScanned == true);
            Debug.Assert(matchingItem.ReconcilingItemTypeCode == 1);

            // Duplicated data?
            Debug.Assert(matchingItem.ScanId == 53);
            Debug.Assert(matchingItem.Scan.Id == 53);

            System.Diagnostics.Trace.WriteLine(result.Count());

            // All tests passed
            System.Diagnostics.Trace.WriteLine("All tests passed.");

        }


    }
}
