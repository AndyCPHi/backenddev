﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using static AndyBackendTest.Model;

namespace AndyBackendTest
{

    // As name suggests for constant data, ie. Location mappings etc.
    public class Constants
    {
        public class GetLocation
        {

            public static string GetLocationNameFromId(int Id)
            {
                var LocationMap = new Dictionary<int, string>(){
                    {1, "London"},
                    {2, "Bristol"},
                    {3, "Wolverhampton"}
                };

                return LocationMap[Id];
            }
        }

        public class GetSite
        {

            public static string GetSiteNameFromId(int Id)
            {
                var siteMap = new Dictionary<int, string>(){
                    {1, "Audi London"},
                    {2, "Renault Bristol"},
                    {3, "Renault Wolverhampton"}
                };

                return siteMap[Id];
            }
        }

        public class SetReconcilingType
        {
            public static ReconcilingItemType GetMissingType()
            {
                var missingItemType = new ReconcilingItemType
                {
                    ExplainsUnknownVehicle = false,
                    ExplainsMissingVehicle = true
                };

                return missingItemType;
            }

            public static ReconcilingItemType GetUnknownType()
            {
                var unknownItemType = new ReconcilingItemType
                {
                    ExplainsUnknownVehicle = true,
                    ExplainsMissingVehicle = false
                };

                return unknownItemType;
            }

        }


    }
}
