﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AndyBackendTest
{
    public class Model
    {
        public class Location
        {
            public int Id;
            public string Description = null;

            public Location(int v1, string v2)
            {
                this.Id = v1;
                this.Description = v2;
            }

        }

        public class StockCheck
        {
            public int Id;
            public DateTime Date;
            public List<StockCheckItem> StockCheckItems;
            public List<Scan> Scans;
            public bool IsTotal;
            public Scan MatchingStockCheckItem;
            public int SiteCode;

            public List<StockCheckItem> GetItemsInStock()
            {
                List<StockCheckItem> results = StockCheckItems.FindAll(item => item.IsInStock);
                return results;
            }

            public List<StockCheckItem> GetMissingVehicles()
            {
                List<StockCheckItem> results = StockCheckItems.FindAll(item => item.ReconcilingItemType.ExplainsMissingVehicle);
                return results;
            }

            public List<StockCheckItem> GetUnknownVehicles()
            {
                List<StockCheckItem> results = StockCheckItems.FindAll(item => item.ReconcilingItemType.ExplainsUnknownVehicle);
                return results;
            }

        }


        public class StockCheckItem
        {
            public int? Id;
            public string Description = null;
            public string Comment = null;
            public string Reg = null;
            public string Chassis = null;

            public bool IsProblem;
            public bool IsFlagForFollowup;
            public bool IsInStock;
            public bool IsScanned;
            public bool IsScannedOrInStock;
            public bool IsResolved;

            public bool MismatchChassis;
            public bool MismatchReg;
            public int ScanId;
            public int StockCheckId;
            public int ReconcilingItemTypeCode;

            public Scan Scan;
            public ReconcilingItemType ReconcilingItemType;
            public StockCheckItem MatchingStockCheckItem;

        public static StockCheckItem CreateDummyItem(int id, string description, string comment, string regId, string chassisName,
                  bool inStock, bool isScanned, ReconcilingItemType recType)
            {
                StockCheckItem newItem = new StockCheckItem
                {
                    Id = id,
                    Description = description,
                    Comment = comment,
                    Chassis = chassisName,
                    Reg = regId,
                    IsInStock = inStock,
                    IsScanned = isScanned,
                    ReconcilingItemType = recType
                };

                return newItem;
            }

        }

        public class Scan
        {
            public int Id;
            public int RegConfidence;
            public string Description = null;
            public string Reg = null;
            public string Chassis = null;
            public int LocationCode;
            public Location Location = null;
            public DateTime ScanDate;
            public ScanSession ScanSession;
            public bool IsDuplicate;
        }

        public class ScanSession
        {
            public int Id;
            public int StockCheckId;

            public ScanSession(int v1, int v2)
            {
                this.Id = v1;
                this.StockCheckId = v2;
            }
        }

        public class Site
        {
            public int Code;
            public string Description;
            public string DescriptionShort;
        }

        public class ReconcilingItemType
        {
            public int Code;
            public string Description;
            public bool ExplainsUnknownVehicle;
            public bool ExplainsMissingVehicle;

        }

    }
}
